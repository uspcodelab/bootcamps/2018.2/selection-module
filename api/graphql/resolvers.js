const resolvers = {
  Query: {

    selectedList: (_, args, context, info) => {
      return context.prisma.query.selecteds({}, info);
    },

    selectedById: (_, args, context, info) => {
      return context.prisma.query.selected({
          where: {
            id: args.searchId
          }
        },
        info)
    },

    registeredList: (_, args, context, info) => {
      return context.prisma.query.registereds({}, info);
    },

    registeredById: (_, args, context, info) => {
      return context.prisma.query.registered({
          where: {
            id: args.searchId
          }
        },
        info)
    }

  },
  Mutation: {
    registerParticipant: (_, args, context, info) => {
      return context.prisma.mutation.createRegistered({
          data: {
            participant_id: args.participant_id
          }
        },
        info
      )
    },

    selectParticipant: (_, args, context, info) => {
      return context.prisma.mutation.createSelected({
          data: {
            registered: {
              connect: {
                id: args.registered_id
              }
            }
          }
        },
        info
      );
    },

    unselectParticipant: (_, args, context, info) => {
      return context.prisma.mutation.deleteSelected({
          where: {
            id: args.id
          }
        },
        info)
    },

    deleteParticipant: (_, args, context, info) => {
      return context.prisma.mutation.deleteRegistered({
          where: {
            id: args.id
          }
        },
        info)
    },

    createSelectedList: (_, args, context, info) => {
      let select = [];
      args.id_array.forEach((id) => {
        select.push(context.prisma.mutation.createSelected({
            data: {
              registered: {
                connect: {
                  id
                }
              }
            }
          },
          info
        ))
      });
      console.log(select);
      return select;
    }

  }
};

export default resolvers;